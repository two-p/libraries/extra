package ch.pagedidier.extra.exception.validation.validator;

import java.util.Collection;
import java.util.Map;

public class NotEmptyValidator extends Validator {
  private static final String CAUSE = "cannot be empty";

  public NotEmptyValidator(String property, String value) {
    super(property, CAUSE, value != null && !value.isEmpty());
  }

  public NotEmptyValidator(String property, Collection<?> value) {
    super(property, CAUSE, value != null && !value.isEmpty());
  }

  public NotEmptyValidator(String property, Map<?, ?> value) {
    super(property, CAUSE, value != null && !value.isEmpty());
  }
}
