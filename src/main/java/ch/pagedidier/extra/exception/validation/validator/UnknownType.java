package ch.pagedidier.extra.exception.validation.validator;

public class UnknownType extends Validator {
  public UnknownType() {
    super("type", "unknown type", false);
  }
}
