package ch.pagedidier.extra.exception.validation.validator;

public class NotNullValidator extends Validator {
  public NotNullValidator(String property, Object value) {
    super(property, "cannot be null", value != null);
  }
}
