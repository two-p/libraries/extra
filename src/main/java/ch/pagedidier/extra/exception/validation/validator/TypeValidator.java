package ch.pagedidier.extra.exception.validation.validator;

public class TypeValidator extends Validator {
  public TypeValidator(String property, String type, String expected) {
    super(
        property,
        String.format("illegal type (current: %s, expected: %s)", type, expected),
        type.equals(expected));
  }
}
