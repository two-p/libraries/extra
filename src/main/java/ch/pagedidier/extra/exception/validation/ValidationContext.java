package ch.pagedidier.extra.exception.validation;

import ch.pagedidier.extra.exception.validation.validator.Validator;
import ch.pagedidier.extra.exception.validation.validator.Validators;

import java.util.LinkedList;
import java.util.List;

import lombok.Getter;

public class ValidationContext {
  @Getter private List<Validator> validators = new LinkedList<>();

  /**
   * Add a validator's validation result to the context if it failed. Succeeding validators are
   * ignored.
   *
   * @param validator A validator.
   * @return Returns the validation context.
   */
  public ValidationContext add(Validator validator) {
    if (validator.isNotValid()) {
      validators.add(validator);
    }

    return this;
  }

  /**
   * Add failing validation results to the context. Succeeding validators are ignored.
   *
   * @param validators A list of validator objects.
   * @return Returns the validation context.
   */
  public ValidationContext add(List<Validator> validators) {
    for (Validator validator : validators) {
      if (validator.isNotValid()) {
        this.validators.add(validator);
      }
    }

    return this;
  }

  /**
   * Add a validators' validation results to the context if they failed. Succeeding validators are
   * ignored.
   *
   * @param validators A validators object.
   * @return Returns the validation context.
   */
  public ValidationContext add(Validators validators) {
    return add(validators.get());
  }

  /**
   * Validates the context.
   *
   * @throws ValidationException When there is at least one failing validator in the context.
   */
  public void validate() throws ValidationException {
    if (!validators.isEmpty()) {
      doThrow();
    }
  }

  public void doThrow() throws ValidationException {
    throw createException();
  }

  public ValidationException createException() {
    return new ValidationException(this);
  }
}
