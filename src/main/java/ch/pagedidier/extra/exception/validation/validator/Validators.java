package ch.pagedidier.extra.exception.validation.validator;

import java.util.LinkedList;
import java.util.List;

public class Validators {
  private final List<Validator> validators = new LinkedList<>();

  protected void add(Validator validator) {
    validators.add(validator);
  }

  public List<Validator> get() {
    return validators;
  }
}
