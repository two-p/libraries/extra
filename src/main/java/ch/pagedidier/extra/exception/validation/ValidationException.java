package ch.pagedidier.extra.exception.validation;

import lombok.Getter;

public class ValidationException extends RuntimeException {

  @Getter private final ValidationContext validationContext;

  public ValidationException(ValidationContext validationContext) {
    super("entity validation error");
    this.validationContext = validationContext;
  }
}
