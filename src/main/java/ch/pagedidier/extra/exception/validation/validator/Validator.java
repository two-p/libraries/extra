package ch.pagedidier.extra.exception.validation.validator;

import lombok.Getter;

public class Validator {
  @Getter private final String property;

  @Getter private final String cause;

  private final boolean isValid;

  public Validator(String property, String cause, boolean isValid) {
    this.property = property;
    this.cause = cause;
    this.isValid = isValid;
  }

  public boolean isValid() {
    return isValid;
  }

  public boolean isNotValid() {
    return !isValid;
  }
}
