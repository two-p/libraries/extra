package ch.pagedidier.extra.exception.validation.validator;

public class NotFound extends Validator {
  public NotFound(String property) {
    super(property, "not found", false);
  }
}
