package ch.pagedidier.extra.exception.validation.validator;

import java.util.Collection;
import java.util.Map;

public class NotNullNotEmptyValidator extends Validators {
  public NotNullNotEmptyValidator(String property, String value) {
    add(new NotNullValidator(property, value));
    add(new NotEmptyValidator(property, value));
  }

  public NotNullNotEmptyValidator(String property, Collection<?> value) {
    add(new NotNullValidator(property, value));
    add(new NotEmptyValidator(property, value));
  }

  public NotNullNotEmptyValidator(String property, Map<?, ?> value) {
    add(new NotNullValidator(property, value));
    add(new NotEmptyValidator(property, value));
  }
}
