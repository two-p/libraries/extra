package ch.pagedidier.extra.domain.mongo;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

@NoArgsConstructor
@AllArgsConstructor
public abstract class MongoModel {

  @Id private ObjectId _id;

  public ObjectId getId() {
    return _id;
  }
}
