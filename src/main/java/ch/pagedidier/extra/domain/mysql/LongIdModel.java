package ch.pagedidier.extra.domain.mysql;

import ch.pagedidier.extra.domain.Model;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;

@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
@Getter
public abstract class LongIdModel extends Model {
  @Id
  @Column(name = "id", nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Field(name = "identifier", analyze = Analyze.NO, store = Store.NO, index = Index.YES)
  @SortableField(forField = "identifier")
  private Long id;
}
