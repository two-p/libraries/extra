package ch.pagedidier.extra.domain.mysql;

import ch.pagedidier.extra.domain.Model;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.annotations.*;

@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
@Getter
public abstract class StringIdModel extends Model {
  @Id
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  @Field(name = "identifier", analyze = Analyze.NO, store = Store.NO, index = Index.YES)
  @SortableField(forField = "identifier")
  private String id;
}
