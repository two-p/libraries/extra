package ch.pagedidier.extra.domain.analyzers;

import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.WhitespaceTokenizerFactory;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilterFactory;
import org.apache.lucene.analysis.miscellaneous.RemoveDuplicatesTokenFilterFactory;
import org.apache.lucene.analysis.ngram.EdgeNGramFilterFactory;
import org.hibernate.search.analyzer.definition.LuceneAnalysisDefinitionProvider;
import org.hibernate.search.analyzer.definition.LuceneAnalysisDefinitionRegistryBuilder;

public class CustomAnalyzerProvider implements LuceneAnalysisDefinitionProvider {
  @Override
  public void register(LuceneAnalysisDefinitionRegistryBuilder builder) {
    builder
        .analyzer("edgeNGram_query")
        .tokenizer(WhitespaceTokenizerFactory.class)
        .tokenFilter(ASCIIFoldingFilterFactory.class)
        .tokenFilter(LowerCaseFilterFactory.class)
        .tokenFilter(RemoveDuplicatesTokenFilterFactory.class);

    builder
        .analyzer("edgeNgram")
        .tokenizer(WhitespaceTokenizerFactory.class)
        .tokenFilter(ASCIIFoldingFilterFactory.class)
        .tokenFilter(LowerCaseFilterFactory.class)
        .tokenFilter(RemoveDuplicatesTokenFilterFactory.class)
        .tokenFilter(EdgeNGramFilterFactory.class)
        .param("minGramSize", "1")
        .param("maxGramSize", "255");
  }
}
