package ch.pagedidier.extra.domain.exception;

public abstract class DomainException extends RuntimeException {
  public DomainException(String message) {
    super(message);
  }

  public DomainException(String messageFormat, Object... args) {
    this(String.format(messageFormat, args));
  }

  public DomainException(String message, Throwable cause) {
    super(message, cause);
  }
}
