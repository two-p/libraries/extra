package ch.pagedidier.extra.domain.exception;

public class UnprocessableEntityException extends DomainException {
  public UnprocessableEntityException(String message) {
    super(message);
  }

  public UnprocessableEntityException(String message, Object... args) {
    super(String.format(message, args));
  }
}
