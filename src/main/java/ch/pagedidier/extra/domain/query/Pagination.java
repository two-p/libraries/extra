package ch.pagedidier.extra.domain.query;

import ch.pagedidier.extra.exception.validation.ValidationContext;
import ch.pagedidier.extra.exception.validation.ValidationException;
import ch.pagedidier.extra.exception.validation.validator.Validator;
import lombok.Data;

@Data
public class Pagination {

  private int page;
  private int size;
  private int pageCount;
  private long elementCount;

  public Pagination(Integer size) throws ValidationException {
    this(0, size);
  }

  public Pagination(int page, int size) throws ValidationException {
    this.page = page;
    this.size = size;

    new ValidationContext()
        .add(new Validator("page", "must be greater than zero", page > 0))
        .add(new Validator("size", "must be greater than zero", size > 0))
        .add(new Validator("size", "must be small than 1000", size < 1000))
        .validate();
  }

  public Pagination(int page, int size, int pageCount, long elementCount)
      throws ValidationException {
    this(page, size);
    this.pageCount = pageCount;
    this.elementCount = elementCount;
  }
}
