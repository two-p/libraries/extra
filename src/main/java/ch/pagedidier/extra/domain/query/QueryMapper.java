package ch.pagedidier.extra.domain.query;

import ch.pagedidier.extra.exception.validation.ValidationException;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class QueryMapper {

  public <T> ch.pagedidier.extra.domain.query.Page<T> toDomain(
      List<T> elements, PaginationModel pagination) throws ValidationException {
    return new ch.pagedidier.extra.domain.query.Page<>(
        elements,
        new Pagination(
            pagination.getPage(), pagination.getSize(),
            pagination.getPageCount(), pagination.getElementCount()));
  }

  public Pagination toDomain(Page<?> page) throws ValidationException {
    return new Pagination(
        page.getNumber() + 1, page.getSize(), page.getTotalPages(), page.getTotalElements());
  }

  public PaginationModel getPaginationModel(Query query) {
    return new PaginationModel(
        query.getPagination().getPage(), query.getPagination().getSize(),
        query.getPagination().getPageCount(), query.getPagination().getElementCount());
  }

  public Pageable getPageable(Query query) {
    Pagination pagination = query.getPagination();
    Sort sort = getSort(query);

    return pagination != null
        ? PageRequest.of(pagination.getPage() - 1, pagination.getSize(), sort)
        : PageRequest.of(0, Integer.MAX_VALUE, sort);
  }

  public Sort getSort(Query query) {
    // TODO: improve domain sort class
    ch.pagedidier.extra.domain.query.Sort domainSort = query.getSort();

    if (domainSort == null) return Sort.unsorted();

    Sort.Order order;
    switch (domainSort.getOrder()) {
      case DESC:
        order = Sort.Order.desc(domainSort.getName());
        break;
      case ASC:
      default:
        order = Sort.Order.asc(domainSort.getName());
    }

    return Sort.by(order);
  }
}
