package ch.pagedidier.extra.domain.query;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class PaginationModel {

  private int page;
  private int size;
  private int pageCount;
  private long elementCount;
}
