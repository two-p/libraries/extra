package ch.pagedidier.extra.domain.query;

import ch.pagedidier.extra.exception.validation.ValidationContext;
import ch.pagedidier.extra.exception.validation.ValidationException;
import ch.pagedidier.extra.exception.validation.validator.NotNullNotEmptyValidator;
import ch.pagedidier.extra.exception.validation.validator.NotNullValidator;
import lombok.Data;

@Data
public class Sort {

  public enum Order {
    ASC,
    DESC
  }

  private String name;
  private Order order;

  public Sort(String name) throws ValidationException {
    this(name, Order.ASC);
  }

  public Sort(String name, Order order) throws ValidationException {
    this.name = name;
    this.order = order;

    new ValidationContext()
        .add(new NotNullNotEmptyValidator("name", name))
        .add(new NotNullValidator("order", order))
        .validate();
  }
}
