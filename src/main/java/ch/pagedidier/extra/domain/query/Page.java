package ch.pagedidier.extra.domain.query;

import ch.pagedidier.extra.exception.validation.ValidationContext;
import ch.pagedidier.extra.exception.validation.ValidationException;
import ch.pagedidier.extra.exception.validation.validator.NotNullValidator;

import java.util.LinkedList;
import java.util.List;

import lombok.Data;

@Data
public class Page<T> {

  private List<T> elements;
  private Pagination pagination;

  public Page(List<T> elements, Pagination pagination) throws ValidationException {
    this.elements = elements;
    this.pagination = pagination;

    new ValidationContext().add(new NotNullValidator("elements", elements)).validate();
  }

  public Page(List<T> elements) throws ValidationException {
    this(elements, new Pagination(1, Math.max(elements.size(), 1), 1, elements.size()));
  }

  public Page() throws ValidationException {
    this(new LinkedList<>());
  }
}
