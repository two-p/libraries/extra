package ch.pagedidier.extra.domain.query;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Query {
  private Pagination pagination;
  private Sort sort;

  public Query(Pagination pagination) {
    this(pagination, null);
  }

  public Query(Pagination pagination, Sort sort) {
    this.pagination = pagination;
    this.sort = sort;
  }
}
