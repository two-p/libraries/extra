package ch.pagedidier.extra.domain.query;

import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class PageModel<T> {
  private List<T> elements;
  private PaginationModel paginationModel;
}
