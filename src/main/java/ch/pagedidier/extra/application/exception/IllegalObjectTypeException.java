package ch.pagedidier.extra.application.exception;

import lombok.Getter;

public class IllegalObjectTypeException extends ApplicationException {
  @Getter private final Class<?> clazz;

  @Getter private final Class<?> expectedClazz;

  public IllegalObjectTypeException(Class<?> clazz, Class<?> expectedClazz) {
    super("received: %s, expected: %s", clazz.getSimpleName(), expectedClazz.getSimpleName());

    this.clazz = clazz;
    this.expectedClazz = expectedClazz;
  }
}
