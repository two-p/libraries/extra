package ch.pagedidier.extra.application.exception.notfound;

import ch.pagedidier.extra.application.exception.ApplicationException;
import lombok.Getter;

public class RelatedObjectNotFoundException extends ApplicationException {

  @Getter private final Class<?> clazz;

  public RelatedObjectNotFoundException(Class<?> clazz) {
    super("related %s not found", clazz.getSimpleName());

    this.clazz = clazz;
  }
}
