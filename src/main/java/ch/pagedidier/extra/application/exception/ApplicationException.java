package ch.pagedidier.extra.application.exception;

public class ApplicationException extends RuntimeException {

  public ApplicationException(String message) {
    super(message);
  }

  public ApplicationException(String message, Object... args) {
    super(String.format(message, args));
  }
}
