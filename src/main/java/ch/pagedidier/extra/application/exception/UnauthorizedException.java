package ch.pagedidier.extra.application.exception;

public class UnauthorizedException extends RuntimeException {

  public UnauthorizedException(String message) {
    super(message);
  }

  public UnauthorizedException(String message, Object... args) {
    super(String.format(message, args));
  }

  public UnauthorizedException(String message, Throwable cause) {
    super(message, cause);
  }
}
