package ch.pagedidier.extra.application.exception.notfound;

import ch.pagedidier.extra.application.exception.ApplicationException;
import lombok.Getter;

public class ObjectNotFoundException extends ApplicationException {

  @Getter private final Class<?> clazz;

  public ObjectNotFoundException(Class<?> clazz) {
    super("%s not found", clazz.getSimpleName());
    this.clazz = clazz;
  }
}
