package ch.pagedidier.extra.application.exception.notfound;

import ch.pagedidier.extra.application.exception.ApplicationException;
import lombok.Getter;

public class EntityNotFoundException extends ApplicationException {

  @Getter private final Object id;

  @Getter private final Class<?> clazz;

  public EntityNotFoundException(Object id, Class<?> clazz) {
    super("%s #%s not found", clazz.getSimpleName(), id);

    this.id = id;
    this.clazz = clazz;
  }
}
