package ch.pagedidier.extra.application.exception;

public class ConflictException extends ApplicationException {

  public ConflictException(String message) {
    super(message);
  }

  public ConflictException(String message, Object... args) {
    super(String.format(message, args));
  }
}
