package ch.pagedidier.extra.infrastructure.exceptions;

public class UnsupportedMediaTypeException extends RuntimeException {

  public UnsupportedMediaTypeException(String message) {
    super(message);
  }

  public UnsupportedMediaTypeException(String message, Object... args) {
    super(String.format(message, args));
  }

  public UnsupportedMediaTypeException(String message, Throwable cause) {
    super(message, cause);
  }
}
