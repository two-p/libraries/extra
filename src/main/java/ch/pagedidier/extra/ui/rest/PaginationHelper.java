package ch.pagedidier.extra.ui.rest;

import ch.pagedidier.extra.domain.query.Pagination;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

@Service
public class PaginationHelper {
  public static final String QUERY_PAGE = "${pagination.query.page}";
  public static final String QUERY_PAGE_DEFAULT = "${pagination.query.page.default}";
  public static final String QUERY_SIZE = "${pagination.query.size}";
  public static final String QUERY_SIZE_DEFAULT = "${pagination.query.size.default}";

  public final String HEADER_PAGE;
  public final String HEADER_SIZE;
  public final String HEADER_PAGES;
  public final String HEADER_ELEMENTS;

  public PaginationHelper(
      @Value("${pagination.headers.page}") String HEADER_PAGE,
      @Value("${pagination.headers.size}") String HEADER_SIZE,
      @Value("${pagination.headers.pages}") String HEADER_PAGES,
      @Value("${pagination.headers.elements}") String HEADER_ELEMENTS) {
    this.HEADER_PAGE = HEADER_PAGE;
    this.HEADER_SIZE = HEADER_SIZE;
    this.HEADER_PAGES = HEADER_PAGES;
    this.HEADER_ELEMENTS = HEADER_ELEMENTS;
  }

  public HttpHeaders responsePaginationHeaders(Pagination pagination) {
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add(HEADER_PAGE, String.valueOf(pagination.getPage()));
    httpHeaders.add(HEADER_SIZE, String.valueOf(pagination.getSize()));
    httpHeaders.add(HEADER_PAGES, String.valueOf(pagination.getPageCount()));
    httpHeaders.add(HEADER_ELEMENTS, String.valueOf(pagination.getElementCount()));

    return httpHeaders;
  }
}
