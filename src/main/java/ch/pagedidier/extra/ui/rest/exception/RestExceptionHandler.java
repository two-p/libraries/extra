package ch.pagedidier.extra.ui.rest.exception;

import ch.pagedidier.extra.application.exception.ApplicationException;
import ch.pagedidier.extra.application.exception.ConflictException;
import ch.pagedidier.extra.application.exception.IllegalObjectTypeException;
import ch.pagedidier.extra.application.exception.notfound.EntityNotFoundException;
import ch.pagedidier.extra.application.exception.notfound.ObjectNotFoundException;
import ch.pagedidier.extra.application.exception.notfound.RelatedObjectNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler({DataIntegrityViolationException.class})
  protected ResponseEntity<?> dataIntegrityViolation(DataIntegrityViolationException exception) {
    // TODO: Translate exception to understandable message?
    return ResponseEntity.unprocessableEntity().build();
  }

  @ExceptionHandler({RuntimeException.class})
  protected ResponseEntity<?> baseExceptionHandler(RuntimeException exception) {
    log.error("{} -> {}", exception.getClass().getSimpleName(), exception.getMessage());
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }

  // Application exceptions

  @ExceptionHandler({ApplicationException.class})
  protected ResponseEntity<?> applicationException(ApplicationException exception) {
    log.error(exception.getMessage());
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
  }

  @ExceptionHandler({ObjectNotFoundException.class})
  protected ResponseEntity<?> applicationException(ObjectNotFoundException exception) {
    log.info(exception.getMessage());
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Error(exception.getMessage()));
  }

  @ExceptionHandler({EntityNotFoundException.class})
  protected ResponseEntity<?> applicationException(EntityNotFoundException exception) {
    log.info(exception.getMessage());
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Error(exception.getMessage()));
  }

  @ExceptionHandler({RelatedObjectNotFoundException.class})
  protected ResponseEntity<?> applicationException(RelatedObjectNotFoundException exception) {
    // return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new
    // Error(exception.getMessage()));
    return UnprocessableEntityExceptionMapper.toResponse(exception);
  }

  @ExceptionHandler({ConflictException.class})
  protected ResponseEntity<?> applicationException(ConflictException exception) {
    return ResponseEntity.status(HttpStatus.CONFLICT).body(new Error(exception.getMessage()));
  }

  @ExceptionHandler({IllegalObjectTypeException.class})
  protected ResponseEntity<?> applicationException(IllegalObjectTypeException exception) {
    return UnprocessableEntityExceptionMapper.toResponse(exception);
  }

  @AllArgsConstructor
  private static class Error {
    @Getter private final String error;
  }
}
