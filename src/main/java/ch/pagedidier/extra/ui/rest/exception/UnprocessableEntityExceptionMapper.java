package ch.pagedidier.extra.ui.rest.exception;

import ch.pagedidier.extra.application.exception.notfound.RelatedObjectNotFoundException;
import ch.pagedidier.extra.application.exception.IllegalObjectTypeException;

import java.util.LinkedList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.ResponseEntity;

public class UnprocessableEntityExceptionMapper {

  public static ResponseEntity<?> toResponse(RelatedObjectNotFoundException exception) {
    Response response = new Response();
    response.add(new Error(exception.getClazz().getSimpleName(), "not found"));
    return ResponseEntity.unprocessableEntity().body(response);
  }

  public static ResponseEntity<?> toResponse(IllegalObjectTypeException exception) {
    Response response = new Response();
    response.add(
        new Error(
            "type",
            String.format(
                "received: %s, expected: %s",
                exception.getClazz().getSimpleName(),
                exception.getExpectedClazz().getSimpleName())));
    return ResponseEntity.unprocessableEntity().body(response);
  }

  private static class Response {
    @Getter private final List<Error> errors = new LinkedList<>();

    public void add(Error error) {
      errors.add(error);
    }
  }

  @AllArgsConstructor
  private static class Error {
    @Getter private final String property;

    @Getter private final String message;
  }
}
